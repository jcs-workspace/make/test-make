CFLAGS = -Wno-implicit-function-declaration

# By default, it begins by processing the first target that does not begin with
#  a . aka the default goal; to do that, it may have to process other targets - 
# specifically, ones the first target depends on.
# 
# See https://stackoverflow.com/questions/2057689/how-does-make-app-know-default-target-to-build-if-no-target-is-specified
all: final
	@echo "This is the default command"

.PHONY: build

final:
	@echo "Linking and producint the final application"
#	gcc $(CFLAGS) main.o hello.c -o final
#	@chmod +x final

build:
	go build

hello.o: hello.c
	@echo "Compiling the hello file"
	gcc $(CFLAGS) -c hello.c

clean:
	@echo "Removing everything but the source files"
	rm main.o hello.o final
